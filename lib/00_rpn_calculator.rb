class RPNCalculator
  # TODO: your code goes here!
  attr_accessor :value
  def initialize
    @value = 0
    @storage = []
  end

  def push(number)
    @storage.push(number)
  end

  def plus
    if @storage.length > 1
      second = @storage.pop
      first = @storage.pop
      @value += first+second 
    elsif @storage.length == 1
      @value += @storage.pop
    elsif @storage.empty?
      raise("calculator is empty")
    end
  end

  def minus
    if @storage.length > 1
      second = @storage.pop
      first = @storage.pop
      @value = first - second
    elsif @storage.empty?
      raise("calculator is empty")
    end
  end

  def times
    if @storage.length > 1
      second = @storage.pop
      first = @storage.pop
      @value = second * first
    elsif @storage.length == 1
      last = @storage.pop
      @value = @value * last
    elsif @storage.empty?
      raise("calculator is empty")
    end
  end

  def divide
    if @storage.length > 1
      second = @storage.pop
      first = @storage.pop
      @value = first.to_f/second.to_f
    elsif @storage.empty?
      raise("calculator is empty")
    end
  end

  def tokens(string)
    operators = ['*','-','+','/']
    array = string.split(' ').map do |value|
      if operators.include?(value)
        value.to_sym
      else
        value.to_i
      end
    end
      array
  end

  def evaluate(string)
    array = tokens(string)
    symbols = []
    array.each do |x|
      if x.class == Symbol
        symbols.push(x)
      else
        @storage.push(x)
      end
    end
    symbols.each do |op|
      if op == :*
        @value = times
      elsif op == :+
        plus
      elsif op == :-
        minus
      elsif op == :/
        divide
      end
    end
    @value
  end

end
